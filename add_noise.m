function noisy_sig = add_noise(sig, SNRdb)
   sig_power = norm(sig)^2 / length(sig);
   sigma2 = sig_power / 10^(SNRdb/10);
   noisy_sig = sig + sqrt(sigma2) * randn(size(sig));
   disp('Noise added to audio file.')
end