my_folder = 'path-to-the-folder-which-contains-the-wav-files';
if ~isdir(my_folder)
  error_message = sprintf('Error: The following folder does not exist:\n%s', my_folder);
  uiwait(warndlg(error_message));
  return;
end

file_pattern = fullfile(my_folder, '*.wav');
files = dir(file_pattern);
fprintf('\nAll files in folder: %d\n\n', length(files));
%return;

% final directory
destination_dir = 'path-to-destination-folder';
if ~exist(destination_dir, 'dir')
   mkdir(destination_dir);
end

% If the noisy audio files is too short, then double the length of it
% while it reaches at least 20 seconds.
% This way, we don't have to cut the speech audio when merging with the bg
% noise, because every speech audio file (chunk) is not longer than
% 10-15 seconds. Most of them are 4-8 seconds.
count_noisy_files = 6;
for k = 1:count_noisy_files
    [y, Fs] = audioread(files(k).name);
    extended_y = [y];
    modified = false;
    while int8((length(extended_y)./Fs) - 1) < int8(20)
        extended_y = [extended_y; y];
        modified = true;
    end
    if modified
        filename = sprintf('%s', files(k).name);
        full_destination = fullfile(my_folder, filename);
        audiowrite(full_destination, extended_y(:,1), 16000);
        fprintf('Files has been modified: %s', files(k).name);
    end    
end

% get noisy files
noisy_files = {1, count_noisy_files};
for k = 1:count_noisy_files
    noisy_files{k} = files(k).name;
    [y, Fs] = audioread(noisy_files{k});
    fprintf('Length of noisy file "%s": %d seconds\n', noisy_files{k}, int8(length(y)./Fs) - 1);
end
    
count_all_clean_files = int16(floor(((length(files) + length(noisy_files)))/2));
fprintf('\nAll clean files in this folder: %d\n', count_all_clean_files);

all_files_count = 1;
all_files_index = 1;
for k = length(noisy_files) + 1 : count_all_clean_files % length(files)
    [y, Fs] = audioread(files(k).name);
    duration_in_seconds = int8(length(y)./Fs) - 1;
    new_file_index = 1;
    
    if duration_in_seconds <= 6
        file_segments = 1;
    elseif duration_in_seconds > 6 && duration_in_seconds <= 12
        file_segments = 2;
    elseif duration_in_seconds > 12 && duration_in_seconds <= 18
        file_segments = 3;
    elseif duration_in_seconds > 18 && duration_in_seconds <= 24
        file_segments = 4;
    else
        file_segments = 5;
    end
    
    segment_duration_in_seconds = int8(duration_in_seconds / file_segments);
    
    for i = 0 : segment_duration_in_seconds : (duration_in_seconds - segment_duration_in_seconds)
        if i == 0
            start = 1;
        else
            start = double(i) * Fs;
        end
        end_sec = double(i + segment_duration_in_seconds);
        samples = [start, end_sec * Fs];
        [y_chunk, Fs] = audioread(files(k).name, samples);
        random_noise_strength = randi([27, 29], 1);
                
        fprintf('#%d  -  %s  -  ', all_files_index, files(k).name);
        fprintf('original duration: %d secs  -  ', duration_in_seconds)
        fprintf('start: %d seconds, end: %d seconds', i, end_sec)
        
        random_background_noise = int8(randi([1, (count_noisy_files + 1)], 1));
        if random_background_noise == 7
            fprintf(', white noise strength: %d  -  ', random_noise_strength)
            noisy_sig = add_noise(y_chunk, random_noise_strength);
        else
            [y_background, bgFs] = audioread(noisy_files{random_background_noise});
            length_of_speech = length(y_chunk);
            length_of_bg_noise = length(y_background);
            min_length = min([length_of_speech, length_of_bg_noise]);
            y_chunk = y_chunk(1 : min_length);
            y_background = y_background(1 : min_length);
            
            if random_background_noise == 1 || random_background_noise == 3
                y_background = y_background * (10^(-20/20));
            else
                y_background = y_background * (10^(-30/20));
            end
            
            noisy_sig = (y_chunk * (10^(0/20)) + (y_background));
            % sound(noisy_sig, 16000)
            fprintf('  -  real noise added: %s\n', noisy_files{random_background_noise})
        end    
        
        filename = sprintf('noisy_magyar_output_%d_%d.wav', int16(k - length(noisy_files)), new_file_index);
        full_destination = fullfile(destination_dir, filename);
        audiowrite(full_destination, noisy_sig(:,1), 16000);
        new_file_index = new_file_index + 1;
        all_files_index = all_files_index + 1;
        
        if k == 1
            plot(noisy_sig)
        end
    end
   
    % mono : noisy_sig(:,1)
    % pause(5)
    % sound(noisy_sig(:,1), 16000)
end

fprintf('\nFiles have been created in the following folder: %s\n', destination_dir)